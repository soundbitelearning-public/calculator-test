<?php

require('vendor/autoload.php');

namespace GCSEPod\Apps;

use Ramsey\Uuid\Uuid;
use SQLite3;

class Calc
{
    function input($input)
    {
        // No input - means we must be showing our summary
        if (empty($input)) {

            // Render an HTML table of all events logged

            $db = new SQLite3('test_database');
            $sql = "SELECT * FROM EVENTS";
            $result = $db->query($sql);
            echo '<table cellspacing="5" border="1">';
            echo '<tr><td>ID</td><td>Session</td><td>Event</td><td>Created At</td></tr>';
            while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
                    echo '<tr>';
                echo '<td>';
                echo implode('</td><td>', $row);
                echo '</td>';
                echo '</tr>';
            }
            echo '</table>';
        } else {
            if ($input['newSession']) { echo (string) Uuid::uuid4();
            } else {

                // Log event to file-based DB

                $db = new SQLite3('test_db');
                $sql = "CREATE TABLE EVENTS
                    (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    `session` VARCHAR(100) NOT NULL,
                    `event` TEXT NOT NULL,
                    created_at DATETIME NOT NULL);";
                $db->exec($sql);

                $sql = "INSERT INTO EVENTS (`session`,`event`,created_at) VALUES (
                    '".$input['session']."',
                    ".$input[input].",
                    ".time().")";

                $result = $db->query($sql);
        }
    }
}

Calc::input($_POST);
