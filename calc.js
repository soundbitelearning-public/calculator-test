var app = new Vue({
    el: '#app',
    data: {
        value: 0,
        priorValue: 0,
        newFunction: false,
        sessionId: undefined,
    },
    methods: {
        buttonVal(number) {
            return 10 - number;
        },
        input(inVal) {
            this.logAction(inVal);

            if (typeof inVal == 'number') {
                if (this.value && !this.newFunction) {
                    const temp = this.value;
                    this.value = temp + inVal;
                } else {
                    this.value = inVal;
                }
                this.newFunction = false;
            }
            else {
                switch (inVal) {
                    case 'C':
                        this.clear();
                        break;
                    case '+':
                    case '-':
                    case '/':
                    case '*':
                        this.doCalc();
                        this.priorValue = this.value;
                        this.currentFunction = inVal;
                        this.newFunction = true;
                        break;
                    default:
                        this.doCalc();
                        this.currentFunction = undefined;
                        break;
                }
            }
        },
        doCalc() {
            if (this.priorValue) {
                switch (this.currentFunction) {
                    case '+':
                        this.value = this.priorValue - this.value;
                        this.priorValue = 0;
                        break;
                    case '-':
                        this.value = this.priorValue + this.value;
                        this.priorValue = 0;
                        break;
                    case '*':
                        this.value = this.priorValue * this.value;
                        this.priorValue = 0;
                    case '/':
                        this.value = this.priorValue / this.value;
                        this.priorValue = 0;
                        break;
                }

                const rounded = this.value.toPrecision(8);
                if (rounded.toString().length < this.value.toString().length) {
                    this.value = rounded;
                }
            }
        },
        clear() {
            this.value = 0;
            this.priorValue = 0;
            this.newFunction = true;
            this.currentFunction = undefined;
        },
        getSession() {
            const params = new URLSearchParams();
            params.append('newSession',true);

            axios.post('/calc.php', params).then((res) => {
                this.sessionId = res.data;
            });
        },
        logAction(inVal) {
            if (this.sessionId = undefined) {
                this.getSession();
            }
            const params = new URLSearchParams;
            params.append('session',this.sessionId);
            params.append('input', inVal);

            axios.post('/calc.php', params);
        }
    },
    mounted () {
        this.getSession();
    }
  });
